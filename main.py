# --*--coding:utf-8--*--

import csv
import json
import codecs
import os

import time

BASE_URL = "/Users/quseit/PycharmProjects/readcsv/开智公开课"

text_md = """---
title:{title}
sign:default
---

# front
{front}


# back
{back}

"""

video_md = """---
title:{title}
sign:default
---

# front

```video
name:{title}
url:{video_url}
```

# back
{back}

"""

exam_md = """---
title:{title}
sign:default
---

# front
{front}

# back
{back}


"""

sq_item_string = """
```SQ
Q: {title}
{item}```
"""

mq_item_string = """
```MQ
Q: {title}
{item}```
"""

with codecs.open(BASE_URL + "/pack_开智公开课.csv",
                 "r", encoding='utf-8') as pack_file:
    for pack_index, pack_item in enumerate(csv.reader(pack_file)):

        with codecs.open(BASE_URL + "/card_开智公开课.csv", "r",
                         encoding="utf-8") as card_file:

            for card_index, card_item in enumerate(csv.reader(card_file)):

                if card_item[8] == pack_item[0]:

                    time.sleep(2)

                    dir_name = pack_item[1]

                    if os.path.exists(BASE_URL + "/" + dir_name):
                        pass
                    else:

                        path_dir = BASE_URL + "/" + dir_name

                        os.mkdir(path_dir)

                    if card_item[2] != 'video' and card_item[2] != "exam" and card_item[2] != "type":
                        time.sleep(1)
                        string = text_md.format(
                            title=card_item[1], front=card_item[3], back=card_item[4])

                        with open(BASE_URL + "/" +
                                  dir_name + "/" + card_item[1] + ".md", "w", encoding='utf-8') as f:
                            f.write(string)
                            f.close()

                    if card_item[2] == "video":
                        time.sleep(1)
                        video_url = json.loads(card_item[6])['video']['url']
                        title = card_item[1]

                        string = video_md.format(
                            title=title, video_url=video_url, back=card_item[4])
                        with open(BASE_URL + "/" +
                                  dir_name + "/" + title + ".video", "w", encoding='utf-8') as f:
                            f.write(string)
                            f.close()

                    if card_item[2] == "exam":
                        time.sleep(1)
                        title = card_item[1]

                        front = card_item[3].split('[')[0].strip('\n')

                        front += '\n'

                        extension = json.loads(card_item[6])

                        exam_items = extension['exam']['item']

                        right_items = extension['exam']['right']

                        sq_item = ""
                        mq_item = ""
                        if len(right_items) < 2:
                            for exam_item in exam_items:
                                if exam_item in right_items:
                                    sq_item += "ASWT: %s\n" % exam_item
                                else:
                                    sq_item += "ASWF: %s\n" % exam_item
                            as_string = sq_item_string.format(
                                title=front, item=sq_item)

                        else:
                            for exam_item in exam_items:

                                if exam_item in right_items:
                                    mq_item += "ASWT: %s\n" % exam_item
                                else:
                                    mq_item += "ASWF: %s\n" % exam_item
                            as_string = mq_item_string.format(
                                title=front, item=mq_item)

                        string = exam_md.format(
                            title=title, front=as_string, back=card_item[4])
                        with open(BASE_URL + "/" + dir_name + "/" + title + ".exam", "w", encoding='utf-8') as f:
                            f.write(string)
                            f.close()
                    if card_item[2] == "type":
                        pass
